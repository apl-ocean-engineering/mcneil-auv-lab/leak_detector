/**
 * Copyright 2023 University of Washington Applied Physics Laboratory
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "leak_detector/leak_detector.h"

#include <wiringpi2/wiringPi.h>

#include "apl_msgs/LeakDetection.h"
#include "ros/ros.h"

LeakDetector::LeakDetector() {
  nh_ = ros::NodeHandle("~");
  nh_.getParam("leak_is_high", leak_is_high_);
  nh_.getParam("wiringpi_pin", wiringpi_pin_);
  nh_.getParam("rate_hz", rate_hz_);

  leak_pub_ = nh_.advertise<apl_msgs::LeakDetection>("status", 10);

  wiringPiSetup();
  pinMode(wiringpi_pin_, INPUT);
}

LeakDetector::~LeakDetector() = default;

void LeakDetector::run() {
  ros::Rate rate = ros::Rate(rate_hz_);
  while (ros::ok()) {
    readLeakPin();
    rate.sleep();
  }
}

void LeakDetector::readLeakPin() {
  int value = digitalRead(wiringpi_pin_);
  bool leak;
  if (leak_is_high_) {
    leak = static_cast<bool>(value);
  } else {
    leak = !static_cast<bool>(value);
  }
  apl_msgs::LeakDetection leak_msg;
  leak_msg.header.stamp = ros::Time::now();
  leak_msg.channel.push_back("leak");
  leak_msg.leak.push_back(leak);
  leak_pub_.publish(leak_msg);
}

int main(int argc, char **argv) {
  ros::init(argc, argv, "leak_detector");
  LeakDetector ld = LeakDetector();
  ld.run();
  return 0;
}
