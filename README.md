## leak_detector

The Remus stretch-limo housing will include a leak detector that provides
a digital input indicating water in the housing.

This is a dead-simple ROS node that opens a given Digital I/O pin and reports
the value as an apl_msgs/DigitalLeak.msg

It has only been tested using an ODroid C4, but I'd expect it to work on a
Raspberry Pi as well, if the correct version of wiringPi is installed.
For running on an ODroid, install HardKernel's fork of wiringPi.


**Parameters**:
* leak_is_high: Whether high voltage on input pin indicates a leak
* wiring_pi_pin: The wiringPi pin number to monitor. This will NOT be the physical pin number; instead, use `gpio readall -a` to figure out the mapping.
* rate_hz: Rate to sample the pin at, in Hz.

**Publications**:
* ~/status (`apl_msgs/DigitalLeak.msg`): single bool indicating whether leak is detected.
